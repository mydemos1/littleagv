from agv import AGV

class AgvLibrary(object):
    def create_new_AGV(self, position="A"):
        self.AGV = AGV(position)

    def move_Up(self):
        self.AGV.moveUp()

    def move_Down(self):
        self.AGV.moveDown()

    def move_Right(self):
        self.AGV.moveRight()

    def move_Left(self):
        self.AGV.moveLeft()

    def position_should_be(self, expected):
        if expected != self.AGV.currentPosition :
            raise AssertionError("Position is not as expected")