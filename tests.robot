*** Settings ***
Documentation   Tests on the AGV Library developed
Library         AgvLibrary.py

*** Test Cases *** 
Go from A to D
    Create new AGV
    Move Right
    Move Right
    Move Up
    Position should be  D

Go from F to B
    Create new AGV  F
    Move Down
    Move Left
    Move Down
    Move Right
    Position should be  B