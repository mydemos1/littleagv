class AGV(object):
    def __init__(self, currentPosition="A"):
        self.currentPosition = currentPosition
        
    def moveUp(self):
        if self.currentPosition == "A":
            self.currentPosition = "G"
        elif self.currentPosition == "C":
            self.currentPosition = "D"
        elif self.currentPosition == "E":
            self.currentPosition = "F"
        else:
            pass

    def moveDown(self):
        if self.currentPosition == "D":
            self.currentPosition = "C"
        elif self.currentPosition == "F":
            self.currentPosition = "E"
        elif self.currentPosition == "G":
            self.currentPosition = "A"
        else:
            pass

    def moveLeft(self):
        if self.currentPosition == "B":
            self.currentPosition = "A"
        elif self.currentPosition == "C":
            self.currentPosition = "B"
        elif self.currentPosition == "D":
            self.currentPosition = "E"
        elif self.currentPosition == "E":
            self.currentPosition = "G"
        else:
            pass
    
    def moveRight(self):
        if self.currentPosition == "G":
            self.currentPosition = "E"
        elif self.currentPosition == "A":
            self.currentPosition = "B"
        elif self.currentPosition == "E":
            self.currentPosition = "D"
        elif self.currentPosition == "B":
            self.currentPosition = "C"
        else:
            pass

